# SimpleStaticSiteGenerator (SSSG)

Made late 2016. Abandoned because of ASP .NET Core Razor Pages are pretty lightweight.

This is very simple CLI program that I made after being frustrated from the fluff
of other "static site generators" that have way more features than I needed.

[Download SimpleStaticSiteGenerator]()

SSSG uses just two partials - `_head.html` and `_end.html`, and inserts that 
content into any `.html` files you have in the `Content\` directory. SSSG also
copies the content of `Content\js\` and `Content\css` with the newly generated
`.html` files into the `wwwroot\` folder.

SSSG also features "placeholders" - which are keys surrounded in two curly braces
in the `.html` files that have text replaced with the key's value in the `App.config`
file.

For example

in _head.html - the title attribute is like so:

    <html>
        ...
        title="{{title}}"
        ...
    
In App.config:

    ...
    <appSettings>
        <add key="index.html" value="{{title}}=IndexPage;" />

This means that the title in `index.html` will be changed from "{{title}}" to "IndexPage".


Usage:

Put your top of the page, like your navigation bar, part into the `_head.html` file

Put the end of the page (footer) part into the `_end.html` file

Then, simply run the SimpleStaticSiteGenerator.exe

Every `.html` file in the `Content\` folder will be generated into the \wwwroot\
folder with the head and end "partials" added

Note: the `.html` file search is not recursive in the `Content\` folder (at
least not yet).


