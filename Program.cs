﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Configuration;


namespace SimpleStaticSiteGenerator
{

    class Program
    {

        //for local/programmer/developer debugging
        //the exe is in the Solution\bin\debug\ folder
#if DEBUG
        private static readonly string CONTENT_DIRECTORY =  @"..\..\Content\";
        private static readonly string LAYOUT_DIRECTORY =  @"..\..\Layout\";
        private static readonly string CSS_DIRECTORY =  @"..\..\Content\css\";
        private static readonly string JAVASCRIPT_DIRECTORY = @"..\..\Content\js\";
        private static readonly string WWWROOT_DIRECTORY = @"..\..\wwwroot\";
#else
        private static readonly string CONTENT_DIRECTORY = Environment.CurrentDirectory + @"Content\";
        private static readonly string LAYOUT_DIRECTORY = Environment.CurrentDirectory + @"Layout\";
        private static readonly string CSS_DIRECTORY = Environment.CurrentDirectory + @"Content\css\";
        private static readonly string JAVASCRIPT_DIRECTORY = Environment.CurrentDirectory + @"Content\js\";
        private static readonly string WWWROOT_DIRECTORY = Environment.CurrentDirectory + @"wwwroot\";
#endif


        /// <summary>
        /// Gets the placeholder names value from the htmlPageName which is the key
        /// </summary>
        /// <param name="htmlPageName">the key</param>
        /// <returns>the key's value</returns>
        internal static string GetPlaceholderValuesFromAppConfig(string htmlPageName)
        {
            return ConfigurationManager.AppSettings[htmlPageName];
        }


        /// <summary>
        /// Parses the App.config file for the placeholder keys and values to replace with in the .html
        /// (content or layout) files
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <param name="appConfigKeysValue"></param>
        /// <returns></returns>
        internal static string ReplaceHtmlPlaceholders(string htmlContent, string appConfigKeysValue)
        {
            string[] itemsSplit = appConfigKeysValue.Split(new char[] { ';', '=' } );

            int itemsToReplace = itemsSplit.Length;

            if (itemsToReplace % 2 != 0)
            {
                throw new Exception("App.config keys are not correct. App.config keys are not evenly split.");
            }

            for (int i = 0; i < itemsToReplace; i+=2)
            {
                string placeholder = itemsSplit[i]; // like {{title}}
                string valueFromConfig = itemsSplit[i + 1]; //like MyIndexPage

                htmlContent = htmlContent.Replace(placeholder, valueFromConfig);
            }

            return htmlContent;
        }


        /// <summary>
        /// Returns all .html file sthat are in the CONTENT_DIRECTORY
        /// </summary>
        /// <returns></returns>
        internal static string[] GetAllHtmlContentFileNames()
        {
            return Directory.GetFiles(CONTENT_DIRECTORY).Where(f => f.ToString().Contains(".html")).ToArray();
        }
        
        /// <summary>
        /// Returns all files in the LAYOUT_DIRECTORY
        /// </summary>
        /// <returns></returns>
        internal static string[] GetAllLayoutFileNames()
        {
            return Directory.GetFiles(LAYOUT_DIRECTORY);
        }

        /// <summary>
        /// Returns all filenames in the CSS_DIRECTORY
        /// </summary>
        /// <returns></returns>
        internal static string[] GetAllCssFiles()
        {
            return Directory.GetFiles(CSS_DIRECTORY);
        }

        /// <summary>
        /// Returns all filenames in the JAVASCRIPT_DIRECTORY
        /// </summary>
        /// <returns></returns>
        internal static string[] GetAllJavascriptFiles()
        {
            return Directory.GetFiles(JAVASCRIPT_DIRECTORY);
        }

        /// <summary>
        /// Copy over the css and js folder's contents into the WWWROOT
        /// Checks to make sure the WWWROOT css and js dirs exists before copying over
        /// </summary>
        internal static void CopyOverCssAndJavascriptFiles()
        {
            if(!Directory.Exists( WWWROOT_DIRECTORY + @"css"))
            {
                Directory.CreateDirectory(WWWROOT_DIRECTORY + @"css\");
            }

            foreach (var file in GetAllCssFiles())
            {
                File.Copy(file, WWWROOT_DIRECTORY + @"css\" + Path.GetFileName(file), overwrite: true); //source, destination- needs to be implemented
            }

            if (!Directory.Exists(WWWROOT_DIRECTORY + @"js"))
            {
                Directory.CreateDirectory(WWWROOT_DIRECTORY + @"js\");
            }

            foreach (var file in GetAllJavascriptFiles())
            {
                File.Copy(file, WWWROOT_DIRECTORY + @"js\" + Path.GetFileName(file), overwrite: true);
            }
        }


        /// <summary>
        /// TODO NOT FINISHED/IMPLEMENTED for like sidebar and stuff
        /// </summary>
        /// <param name="html"></param>
        /// <param name="layoutPlaceholdername"></param>
        /// <param name="layoutContents"></param>
        /// <returns></returns>
        internal static string InsertLayout(string html, string layoutPlaceholdername, string layoutContents)
        {
            html = html.Replace(layoutPlaceholdername, layoutContents);
            return html;
        }


        /// <summary>
        /// Main entry point for the program.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Console.WriteLine("Simple Static Site Generator");
            Console.WriteLine("The generated .html files, and the css and js files it relies on, will be placed into the wwwroot directory.");

            DateTime dateStart = DateTime.Now;

            bool errorHappened = false;

            try
            {
                CreateDirectories();

                DeleteOldGeneratedFiles();

                GenerateHtmlFiles();

                CopyOverCssAndJavascriptFiles();

                //Show index page
                if (Directory.Exists(WWWROOT_DIRECTORY) && File.Exists(WWWROOT_DIRECTORY + "index.html"))
                {
                    Process.Start(WWWROOT_DIRECTORY + @"index.html");
                }
            }
            catch (Exception ex)
            {
                errorHappened = true;
                Console.WriteLine(ex.Message);
            }

            if(errorHappened)
            {
                Console.WriteLine(@"Error occurred: Please check your Content\ and Layout\ folders for errors, or the App.config file for invalid placeholder values.");
            }
            else
            {
                Console.WriteLine("Total time taken: " + (DateTime.Now - dateStart).TotalSeconds + " seconds" + Environment.NewLine);
                Console.WriteLine(Environment.NewLine + "Done! Press enter to exit." + Environment.NewLine + "-----------------------------------");
            }
            //wait for user input.
            Console.Read();
        }

        /// <summary>
        /// Generate all .html files in the Content directory with the layouts of _head.html and _end.html
        /// These files are placed into the wwwroot directory.
        /// </summary>
        private static void GenerateHtmlFiles()
        {
            Console.WriteLine("Generating...");

            var headHtmlContents = File.ReadAllText(LAYOUT_DIRECTORY + "_head.html");

            var endHtmlContents = File.ReadAllText(LAYOUT_DIRECTORY + "_end.html");

            foreach (var htmlFile in GetAllHtmlContentFileNames())
            {
                var bodyHtml = File.ReadAllText(htmlFile);

                var htmlBuilder = new StringBuilder();

                string completeHtml = "";

                htmlBuilder.Append(headHtmlContents);
                htmlBuilder.Append(bodyHtml);
                htmlBuilder.Append(endHtmlContents);

                string placeholders = GetPlaceholderValuesFromAppConfig(Path.GetFileName(htmlFile));

                //check to make sure that the key=htmlFileName exists before calling ReplaceHtmlPlaceholders()
                if(placeholders != null)
                {
                    completeHtml = ReplaceHtmlPlaceholders(htmlBuilder.ToString(), placeholders);
                }
                //default the {{title}} to the file name
                else if (headHtmlContents.Contains("{{title}}"))
                {
                    completeHtml = ReplaceHtmlPlaceholders(htmlBuilder.ToString(), "{{title}}=" + Path.GetFileNameWithoutExtension(htmlFile));
                }

                File.WriteAllText(WWWROOT_DIRECTORY + Path.GetFileName(htmlFile), completeHtml);
            }
        }


        /// <summary>
        /// Creates directories for content if they do not exist.
        /// </summary>
        private static void CreateDirectories()
        {
            if (!Directory.Exists(WWWROOT_DIRECTORY))
            {
                Directory.CreateDirectory(WWWROOT_DIRECTORY);
            }
            if (!Directory.Exists(CSS_DIRECTORY))
            {
                Directory.CreateDirectory(CSS_DIRECTORY);
            }
            if (!Directory.Exists(JAVASCRIPT_DIRECTORY))
            {
                Directory.CreateDirectory(JAVASCRIPT_DIRECTORY);
            }
        }


        /// <summary>
        /// Deletes the old generated files in the wwwroot\ folder.
        /// </summary>
        private static void DeleteOldGeneratedFiles()
        {
            Console.WriteLine($"Deleting all contents of {WWWROOT_DIRECTORY} and building the static files in {CONTENT_DIRECTORY} into {WWWROOT_DIRECTORY}");
            try
            {
                foreach (var filename in Directory.GetFiles(WWWROOT_DIRECTORY))
                {
                    File.Delete(filename);
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
